package bulletinBoard;

import java.util.Arrays;

import dao.BulletinBoardDAO;
import dto.Comment;
import util.KbScanner;
import util.Operation;
import util.StringCheck;

/**
 * 掲示板クラス
 * @author AZUMA Ginji
 */
public class Board {
	private int[] showRange; // 何番目のコメントから何番目のコメントまでを表示しているか
	private final int W_SIZE = Operation.getWindowSize(); // 1回で表示するコメント数

	public Board() {
		// DB から最新の wSize 件のコメントの範囲を取得
		showRange = new int[2];
		int cnt = -1;
		cnt = BulletinBoardDAO.cntComments();
		if (cnt > 0) {
			showRange[0] = Math.max(1, cnt - (W_SIZE - 1));
			showRange[1] = cnt;
		} else {
			Arrays.fill(showRange, 0);
		}
	}

	/** 現在表示しているよりも前のコメントが存在するか
	 * @return true: 存在する、false: 存在しない
	 */
	public boolean hasFore() {
		return BulletinBoardDAO.hasForeComments(showRange[0]);
	}

	/** 現在表示しているよりも後のコメントが存在するか
	 * @return true: 存在する、false: 存在しない
	 */
	public boolean hasNext() {
		// DB に showRange[1] 番目より後のコメントが存在するか確認
		return BulletinBoardDAO.hasNextComments(showRange[1]);
	}

	/** ログイン処理
	 * @param id: ログイン ID
	 * @param pass: ログインパスワード
	 * @return true: ログイン成功, false: ログイン失敗
	 */
	public boolean login(String id, String pass) {
		return BulletinBoardDAO.checkLogin(id, pass);
	}
	
	/** 新規ユーザ登録
	 * @param id: 登録 ID
	 * @param name: 登録ユーザ名
	 * @param pass: 登録 PASS
	 * @return true: 登録成功, false: 登録失敗
	 */
	public boolean register(String id, String name, String pass) {
		boolean result = false;
		if (BulletinBoardDAO.checkUser(id)) {
			System.out.println("! 指定のIDは既に使用されています !");
		} else {
			result = BulletinBoardDAO.registerUser(id, name, pass);
			if (!result) {
				System.out.println("! 登録に失敗しました !");
			}
		}
		return result;
	}

	/** showRange の範囲のコメントを表示 
	 * @return 処理の成否
	 */
	public boolean showComment() {
		boolean result = false;
		Comment[] comments = BulletinBoardDAO.getComment(showRange[0], showRange[1]);
		System.out.println();
		System.out.println("短文投稿掲示板");
		System.out.println("----");
		for (Comment cmt : comments) {
			if (cmt != null) {
				System.out.println(cmt);
				System.out.println();
			}
		}
		System.out.println("----");
		System.out.println();
		result = true;
		return result;
	}

	/** 投稿内容をチェックしてアップロード
	 * @return 処理の成否
	 */
	public boolean upLoad(String id) {
		boolean result = false;
		String comment;
		do {
			// 投稿内容を取得
			System.out.println("投稿内容を入力してください (140文字以内)");
			comment = StringCheck.inputString();
		} while (!StringCheck.checkSentence(comment));

		// 投稿内容を DB にアップロード
		result = BulletinBoardDAO.upComment(id, comment);
		// 表示範囲を投稿内容を含む最新の W_SIZE 件に更新
		this.showRange[1] = BulletinBoardDAO.cntComments();
		if (this.showRange[1] > W_SIZE) {
			this.showRange[0] = this.showRange[1] - (W_SIZE - 1);
		} else {
			this.showRange[0] = 1;
		}
		return result;
	}

	/** 表示しているよりも wSize 件前の投稿をチェック */
	public void showFore() {
		if (hasFore()) {
			showRange[0] = Math.max(showRange[0] - W_SIZE, 1);
			showRange[1] = showRange[0] + (W_SIZE - 1);
		} else {
			System.out.println("! これより前の投稿はありません !");
		}
	}

	/** 表示しているよりも wSize 件後の投稿をチェック */
	public void showNext() {
		if (hasNext()) {
			showRange[0] = showRange[0] + W_SIZE;
			showRange[1] = showRange[0] + (W_SIZE - 1);
		} else {
			System.out.println("! これより後の投稿はありません !");
		}
	}

	/** 自身の投稿内容を削除 
	 * @param id: ユーザID
	 * @return 処理の成否
	 */
	public boolean delete(String id) {
		boolean result = false;
		int target;
		boolean flg = true;
		
		do {
			target = KbScanner.getInt("削除する投稿内容の番号を入力(0 でキャンセル): ");
			if (target == 0) {
				return true;
			}
			flg = (target < 0 || BulletinBoardDAO.cntComments() < target);
			if (flg) {
				System.out.println("! 不正な値です !");
			}
		} while (flg);
		// ユーザと投稿内容(target番目のコメント)の id が同じであれば、投稿内容を ""(空文字列)で上書き
		Comment[] comments = BulletinBoardDAO.getComment(target, target);
		if (comments.length == 1) {
			if (id.equals(comments[0].getID())) {
				// 投稿内容を削除
				result = BulletinBoardDAO.deleteComment(target);
			} else {
				System.out.println("! 削除できるのは自分の投稿内容のみです !");
			}
		}
		return result;
	}
	
	/** ID に紐づいたユーザ名を取得する
	 * @param id: ユーザID
	 * @return ユーザ名
	 */
	public String getName(String id) {
		return BulletinBoardDAO.getUserName(id);
	}
}
