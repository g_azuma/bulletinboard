package bulletinBoard;

import dao.BulletinBoardDAO;
import util.KbScanner;
import util.Operation;

public class Main {

	public static void main(String[] args) {
		// DBとのコネクション確立
		if (!BulletinBoardDAO.connect()) {
			System.out.println("データベースに接続することができませんでした");
			System.out.println("終了します");
			System.exit(1);
		}
		User user = new User();
		int selected = -1;
		
		// 「終了」が選択されるまで掲示板を操作する
		while (true) {
			user.showComment();
			do {
				user.showMenu();
				selected = KbScanner.getInt();
			} while (!user.checkOrder(selected));
			if (selected == Operation.EXIT.ordinal()) {
				System.out.println("終了します");
				break;
			}
			user.useBoard(selected);
		}
		BulletinBoardDAO.closeConnect(); // DB切断
	}
}
