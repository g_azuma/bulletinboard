package bulletinBoard;
/**
 * 短文投稿掲示板のユーザ
 * @author user
 */

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import util.KbScanner;
import util.Operation;

public class User {
	private String id;
	private String name;
	private boolean isLogin;
	private Board board;
	private boolean[] canSelect;
	
	public User() {
		this.id = "";
		this.name = "";
		this.isLogin = false;
		this.board = new Board();
		this.canSelect = new boolean[Operation.values().length];
		Arrays.fill(canSelect, true);
		canSelect[Operation.UP_LOAD.ordinal()] = isLogin;
		canSelect[Operation.DELETE.ordinal()] = isLogin;
		canSelect[Operation.LOGOUT.ordinal()] = isLogin;
		if (!board.hasFore())  canSelect[Operation.SHOW_FORE.ordinal()] = false;
		if (!board.hasNext())  canSelect[Operation.SHOW_NEXT.ordinal()] = false;
	}
	
	/** ログイン処理
	 * @return ture: ログイン成功, false: ログイン失敗
	 */
	public boolean login() {
		String id = KbScanner.getStr("ID: ");
		String pass = KbScanner.getPass("PASS: ");
		/* ID と PASS を渡してログイン */
		if (board.login(id, pass)) {
			this.isLogin = true;
			this.id = id;
			this.name = board.getName(id);
			canSelect[Operation.UP_LOAD.ordinal()] = isLogin;
			canSelect[Operation.DELETE.ordinal()] = isLogin;
		} else {
			System.out.println("! ID またはパスワードが違います !");
		}
		return isLogin;
	}
	
	/** 新規ユーザ登録
	 * @return true: 登録成功, false: 登録失敗
	 */
	public boolean register() {
		String id = "", name = "", pass = "", copyPass = "";
		do {
			id = KbScanner.getStr("登録ID (50文字以下): ");
			if (id.length() > 50) {
				System.out.println("! 50文字以下にしてください !");
				continue;
			} else if (id.length() == 0) {
				System.out.println("! 登録するIDを入力してください !");
				continue;
			}
		} while (id.length() > 50 || id.length() == 0);
		do {
			name = KbScanner.getStr("登録ユーザ名 (50文字以下): ");
			if (name.length() > 50) {
				System.out.println("! 50文字以下にしてください !");
				continue;
			} else if (name.length() == 0) {
				System.out.println("! 登録するユーザ名を入力してください !");
				continue;
			}
		} while (name.length() > 50 || name.length() == 0);
		do {
			pass = KbScanner.getPass("登録PASS (20文字以下): ");
			if (pass.length() > 20) {
				System.out.println("! 20文字以下にしてください !");
				continue;
			} else if (pass.length() == 0) {
				System.out.println("! パスワードを設定してください !");
				continue;
			}
			copyPass = KbScanner.getPass("登録PASS(再入力): ");
			if (!pass.equals(copyPass)) {
				System.out.println("! パスワードが一致しません !");
				continue;
			}
		} while (pass.length() > 20 || pass.length() == 0 || !pass.equals(copyPass));
		isLogin = board.register(id, name, pass);
		if (isLogin) {
			this.id = id;
			this.name = name;
			canSelect[Operation.UP_LOAD.ordinal()] = isLogin;
			canSelect[Operation.DELETE.ordinal()] = isLogin;
		}
		return isLogin;
	}

	/** 選択した操作が可能かをチェック
	 * @param order: 選択した操作
	 * @return true: 可能, false: 不可
	 */
	public boolean checkOrder(int order) {
		boolean ret = false;
		if (0 <= order && order < canSelect.length) {
			ret = canSelect[order];
		}
		return ret;
	}
	
	/** メニューの表示 */
	public void showMenu() {
		canSelect[Operation.UP_LOAD.ordinal()] = isLogin;
		canSelect[Operation.SHOW_FORE.ordinal()] = board.hasFore();
		canSelect[Operation.SHOW_NEXT.ordinal()] = board.hasNext();
		canSelect[Operation.DELETE.ordinal()] = isLogin;
		canSelect[Operation.LOGIN.ordinal()] = !isLogin;
		canSelect[Operation.REGISTER.ordinal()] = !isLogin;
		canSelect[Operation.LOGOUT.ordinal()] = isLogin;
		for (Operation order : Operation.values()) {
			if (canSelect[order.ordinal()]) {
				System.out.print(order.ordinal() + ":" + order.getOrder() + "  ");
			}
		}
		System.out.println();
		if (this.isLogin) {
			System.out.println("[ID:" + this.id + "  ユーザ名:" + this.name + "]");
		}
		System.out.print("操作番号を選択: ");
	}

	/** 投稿内容の表示 */
	public void showComment() {
		board.showComment();
	}
	
	/** 掲示板の利用
	 * @return true: 操作完了, false: 操作失敗
	 */
	public boolean useBoard(int select) {
		boolean ret = true;
		int upLoad = Operation.UP_LOAD.ordinal();
		int showFore = Operation.SHOW_FORE.ordinal();
		int showNext = Operation.SHOW_NEXT.ordinal();
		int delete = Operation.DELETE.ordinal();
		int login = Operation.LOGIN.ordinal();
		int register = Operation.REGISTER.ordinal();
		int logout = Operation.LOGOUT.ordinal();
		if (select == upLoad && canSelect[upLoad]) {
			ret = board.upLoad(this.id);
		} else if (select == showFore && canSelect[showFore]) {
			board.showFore();
		} else if (select == showNext && canSelect[showNext]) {
			board.showNext();
		} else if (select == delete && canSelect[delete]) {
			ret = board.delete(this.id);
		} else if (select == login && canSelect[login]) {
			ret = login();
		} else if (select == register && canSelect[register]) {
			ret = register();
		} else if (select == logout && canSelect[logout]) {
			this.isLogin = false;
			this.id = "";
			this.name = "";
		} else {
			ret = false;
		}
		// エラーメッセージ確認のため、2秒ほど処理を止める
		if (!ret) {
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
}
