package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import dto.Comment;

public class BulletinBoardDAO {
	private static Connection con = null;
	private static final String DB_URL =  "jdbc:mysql://localhost:3306/bulletin_board?serverTimezone=JST";
	private static final String DB_USER = "board_user";
	private static final String DB_PASS = "1234";
	
	// インスタンス化を禁止
	private BulletinBoardDAO() {}
	
	/** DB へのコネクションを確立
	 * @return true:確立, false:失敗
	 */
	public static boolean connect() {
		boolean ret = false;
		try {
			con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
			ret = true;
		} catch (SQLException e) {
			System.out.println("! データベースへの接続に失敗 !");
		}
		return ret;
	}
	
	/** DB への接続を切断 */
	public static void closeConnect() {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			System.out.println("! データベースとのコネクションの切断に失敗 !");
		}
	}
	
	/** コメントの総数を取得
	 * @return コメント総数
	 */
	public static int cntComments() {
		int cnt = -1;
		try {
			String sql = "SELECT COUNT(*) FROM comments;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			cnt = rs.getInt("count(*)");
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return cnt;
	}
	
	/** index 番目より前のコメントが存在するか
	 * @param index
	 * @return true:存在する, false:存在しない
	 */
	public static boolean hasForeComments(int index) {
		boolean flg = false;
		try {
			String sql = "SELECT COUNT(*) FROM comments WHERE cnt < ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, index);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt("count(*)") > 0) {
				flg = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return flg;
	}

	/** index 番目より後のコメントが存在するか
	 * @param index
	 * @return true:存在する, false:存在しない
	 */
	public static boolean hasNextComments(int index) {
		boolean flg = false;
		try {
			String sql = "SELECT COUNT(*) FROM comments WHERE cnt > ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, index);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt("count(*)") > 0) {
				flg = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return flg;
	}

	/** 指定の ID と PASS でログイン可能かを調べる
	 * @param id: ユーザ ID
	 * @param pass: パスワード
	 * @return true:存在する, false:存在しない
	 */
	public static boolean checkLogin(String id, String pass) {
		boolean flg = false;
		try {
			String sql = "SELECT COUNT(*) FROM users WHERE id = ? AND pass = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, pass);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt("count(*)") == 1) {
				flg = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return flg;
	}
	
	/** 指定の ID のユーザが存在するか確認する
	 * @param id: ユーザ ID
	 * @param pass: パスワード
	 * @return true:存在する, false:存在しない
	 */
	public static boolean checkUser(String id) {
		boolean flg = false;
		try {
			String sql = "SELECT COUNT(*) FROM users WHERE id = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt("count(*)") == 1) {
				flg = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return flg;
	}

	/** ユーザ登録
	 * @param id: ユーザID
	 * @param name: ユーザ名
	 * @param pass: パスワード
	 * @return true:成功, false:失敗
	 */
	public static boolean registerUser(String id, String name, String pass) {
		boolean result = false;
		try {
			String sql = "INSERT INTO users VALUES (?, ?, ?);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, name);
			pstmt.setString(3, pass);
			pstmt.executeUpdate();
			pstmt.close();
			result = true;
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return result;
	}

	/** 指定した範囲のコメントを取得する
	 * @param head: 範囲の先頭
	 * @param tail: 範囲の末尾
	 * @return Commentインスタンス群
	 */
	public static Comment[] getComment(int head, int tail) {
		int cmtSize = tail - head + 1;
		Comment[] comments = new Comment[cmtSize];
		try {
			String sql = "SELECT * FROM comments"
					+ " LEFT OUTER JOIN users ON comments.id = users.id"
					+ " WHERE ? <= cnt AND cnt <= ?"
					+ " ORDER BY cnt ASC;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, head);
			pstmt.setInt(2, tail);
			ResultSet rs = pstmt.executeQuery();
			// コメント群を取得
			int i = 0;
			while (rs.next()) {
				int cnt = rs.getInt("cnt");
				String userName = rs.getString("name");
				String id = rs.getString("id");
				String date = new SimpleDateFormat("yyyy/MM/dd HH:mm").format(rs.getTimestamp("date"));
				String comment = rs.getString("comment");
				comments[i] = new Comment(cnt, userName, id, date, comment);
				i++;
			}
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return comments;
	}

	/** 指定した番号のコメントを投稿したユーザのIDを取得
	 * @param cnt: コメント番号
	 * @return コメントを投稿したユーザのID
	 */
	public static String getCommentID(int cnt) {
		String cmtID = null;
		Comment[] comment = getComment(cnt, cnt);
		cmtID = comment[0].getID();
		return cmtID;
	}
	
	/** コメントの投稿
	 * @param id: ユーザID
	 * @param comment: 投稿内容
	 * @return true:成功, false:失敗
	 */
	public static boolean upComment(String id, String comment) {
		boolean result = false;
		try {
			String sql = "INSERT INTO comments (id, comment) VALUES (?, ?);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, comment);
			pstmt.executeUpdate();
			pstmt.close();
			result = true;
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return result;
	}

	/** 指定した番号のコメントを削除
	 * @param cnt: コメント番号
	 * @return true:成功, false:失敗
	 */
	public static boolean deleteComment(int cnt) {
		boolean result = false;
		try {
			String sql = "UPDATE comments SET comment = '' WHERE cnt = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, cnt);
			pstmt.executeUpdate();
			pstmt.close();
			result = true;
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return result;
	}
	
	/** ID に紐づいたユーザ名を取得する
	 * @param id: ユーザID
	 * @return ユーザ名
	 */
	public static String getUserName(String id) {
		String name = null;
		try {
			String sql = "SELECT name FROM users WHERE id = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			name = rs.getString("name");
			pstmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("! データベースとの接続に失敗 !");
		} catch (NullPointerException e) {
			System.out.println("! データベースに接続されていません !");
		}
		return name;
	}
}
