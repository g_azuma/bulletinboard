package dto;

public class Comment {
	private int cnt;
	private String userName;
	private String id;
	private String date;
	private String comment;
	
	public Comment(int cnt, String userName, String id, String date, String comment) {
		this.cnt = cnt;
		this.userName = userName;
		this.id = id;
		this.date = date;
		this.comment = (comment.equals("")) ? "[投稿者によって削除されました]" : comment;
	}
	
	public String getID() {
		return this.id;
	}
	
	/** Comment の配列を cnt の昇順に並び替え
	 * @param comments: Comment の配列
	 * @return cnt で昇順にソートされた comments
	 */
	public static Comment[] sort(Comment[] comments) {
		int comments_size = 0;
		for (Comment cmt : comments) {
			if (cmt != null)  comments_size++;
		}
		for (int i = 0; i < comments_size - 1; i++) {
			for (int j = comments_size - 1; j > i; j--) {
				if (comments[j - 1].cnt > comments[j].cnt) {
					Comment tmp = comments[j - 1];
					comments[j - 1] = comments[j];
					comments[j] = tmp;
				}
			}
		}
		return comments;
	}
	
	public String toString() {
		return cnt + ": " + userName + "@" + id + "    " + date + "\n" + comment;
	}
}
