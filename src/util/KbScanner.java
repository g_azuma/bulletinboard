package util;

import java.io.Console;
import java.util.InputMismatchException;
import java.util.Scanner;

public class KbScanner {
	private static Scanner scanner = new Scanner(System.in);
	private static Console console = System.console();
	
	public static int getInt() {
		int i = -1;
		try {
			i = scanner.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("! 数値で選択してください !");
		} finally {
			scanner.nextLine();
		}
		return i;
	}
	
	public static int getInt(String prompt) {
		System.out.print(prompt);
		return getInt();
	}
	
	public static String getStr() {
		return scanner.nextLine();
	}
	
	public static String getStr(String prompt) {
		System.out.print(prompt);
		return getStr();
	}
	
	/** パスワードをキーボード入力から取得
	 * @param prompt: プロンプト
	 * @return 入力文字列
	 */
	public static String getPass(String prompt) {
		try {
			char[] pass = console.readPassword(prompt);
			return String.valueOf(pass);
		} catch (NullPointerException e) {
			// console が null (eclipse) ならば、素直に System.in を使用
			// この場合、入力したパスワードが表示される
			System.out.print(prompt);
			return getStr();
		}
	}
}
