package util;

public enum Operation {
	UP_LOAD("投稿"),
	SHOW_FORE("前の5件を表示"),
	SHOW_NEXT("次の5件を表示"),
	DELETE("過去の投稿内容を削除"),
	LOGIN("ログイン"),
	REGISTER("ユーザ登録"),
	LOGOUT("ログアウト"),
	EXIT("終了");

	private final String order;
	
	Operation(String order) {
		this.order = order;
	}
	
	public String getOrder() {
		return order;
	}
	
	/** 1回で表示する投稿内容数を返す */
	public static int getWindowSize() {
		return 5;
	}
}
