package util;

public class StringCheck {
	/**
	 * 単文を入力させるメソッド
	 * 文の前後の空白(半角、全角問わず)は削除
	 * @return 入力内容
	 */
	public static String inputString() {
		String str = KbScanner.getStr();
		str = str.replaceAll("^[\\s　]*|[\\s　]*$", ""); // 前後の空白を削除
		return str;
	}

	/**
	 * 入力内容が投稿可能か判定
	 * @param 入力内容
	 * @return 判定結果 (true: 有効, false: 無効)
	 */
	public static boolean checkSentence(String str) {
		boolean flg = str.matches("^[\\s　]*[^\\s　].{0,138}[^\\s　]?[\\s　]*$");
		if (!flg) {
			if (str.matches("^[\\s　]*$")) {
				System.out.println("何も入力されていません");
			} else {
				System.out.println("投稿文が長すぎます");
			}
		}
		return flg;
	}
}